<?php
namespace AppBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="deplacement")
 */
class deplacement {
    
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\Column(type="string", length=100)
     */
    public $nom;
    
    /**
     * @ORM\Column(type="date", length=50)
     */
    public $datedepart;
    /**
     * @ORM\Column(type="time", length=50)
     */
    public $heuredepart;
    
    
    /**
     * @ORM\Column(type="date", length=1000)
     */
    public $dateretour;
    /**
     * @ORM\Column(type="time", length=50)
     */
    public $heureretour;
    /**
     * @ORM\OneToOne(targetEntity=Ville::class, )
     *
     */
    public $ville;
    /**
     * @ORM\OneToOne(targetEntity=Moyen::class, )
     * 
     */
    public $moyen;
    
    
    
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId() { return $this->id; }
    
    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return deplacement
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
        
        return $this;
    }
    
    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }
    
    /**
     * Set datedepart
     *
     * @param string $datedepart
     *
     * @return deplacement
     */
    public function setDatedepart($datedepart)
    {
        $this->datedepart = $datedepart;
        
        return $this;
    }
    
    /**
     * Get datedepart
     *
     * @return string
     */
    public function getDatedepart()
    {
        return $this->datedepart;
    }
    
    /**
     * Set dateretour
     *
     * @param string $dateretour
     *
     * @return deplacement
     */
    public function setDateretour($dateretour)
    {
        $this->dateretour = $dateretour;
        
        return $this;
    }
    
    /**
     * Get dateretour
     *
     * @return string
     */
    public function getDateretour()
    {
        return $this->dateretour;
    }
    
    /**
     * Set moyen
     *
     * @param string $moyen
     *
     * @return deplacement
     */
    public function setMoyen($moyen_id)
    {
        $this->moyen = $moyen_id;
        
        return $this;
    }
    
    /**
     * Get moyen
     *
     * @return string
     */
    public function getMoyen()
    {
        return $this->moyen;
    }
    
    /**
     * Set heuredepart
     *
     * @param \DateTime $heuredepart
     *
     * @return deplacement
     */
    public function setHeuredepart($heuredepart)
    {
        $this->heuredepart = $heuredepart;
        
        return $this;
    }
    
    /**
     * Get heuredepart
     *
     * @return \DateTime
     */
    public function getHeuredepart()
    {
        return $this->heuredepart;
    }
    
    /**
     * Set heureretour
     *
     * @param \DateTime $heureretour
     *
     * @return deplacement
     */
    public function setHeureretour($heureretour)
    {
        $this->heureretour = $heureretour;
        
        return $this;
    }
    
    /**
     * Get heureretour
     *
     * @return \DateTime
     */
    public function getHeureretour()
    {
        return $this->heureretour;
    }
    
    /**
     * Set ville
     *
     * @param string $ville
     *
     * @return deplacement
     */
    public function setVille($ville)
    {
        $this->ville = $ville;
        
        return $this;
    }
    
    /**
     * Get ville
     *
     * @return string
     */
    public function getVille()
    {
        return $this->ville;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->ville_id = new \Doctrine\Common\Collections\ArrayCollection();
        $this->moyen_id = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add idVille
     *
     * @param \AppBundle\Entity\Ville $idVille
     *
     * @return deplacement
     */
    public function addVille(\AppBundle\Entity\Ville $id)
    {
        $this->ville_id[] = $id;

        return $this;
    }

    /**
     * Remove ville_id
     *
     * @param \AppBundle\Entity\Ville $ville_id
     */
    public function removeVille(\AppBundle\Entity\Ville $id)
    {
        $this->idVille->removeElement($id);
    }

    /**
     * Add moyen_id
     *
     * @param \AppBundle\Entity\Moyen $id
     *
     * @return deplacement
     */
    public function addMoyen(\AppBundle\Entity\Moyen $id)
    {
        $this->moyen_id[] = $id;

        return $this;
    }

    /**
     * Remove moyen_id
     *
     * @param \AppBundle\Entity\Moyen $id
     */
    public function removeMoyen(\AppBundle\Entity\Moyen $id)
    {
        $this->moyen_id->removeElement($id);
    }

    /**
     * Set ville_id
     *
     * @param \AppBundle\Entity\Ville $id
     *
     * @return deplacement
     */
    public function setIdVille(\AppBundle\Entity\Ville $id = null)
    {
        $this->ville_id = $id;

        return $this;
    }

    /**
     * Get ville_id
     *
     * @return \AppBundle\Entity\Ville
     */
    public function getIdVille()
    {
        return $this->ville_id;
    }

    /**
     * Set moyen_id
     *
     * @param \AppBundle\Entity\Moyen $id
     *
     * @return deplacement
     */
    public function setIdMoyen(\AppBundle\Entity\Moyen $id = null)
    {
        $this-> moyen_id = $id;

        return $this;
    }

    /**
     * Get moyen_id
     *
     * @return \AppBundle\Entity\Moyen
     */
    public function getIdMoyen()
    {
        return $this->moyen_id;
    }
}

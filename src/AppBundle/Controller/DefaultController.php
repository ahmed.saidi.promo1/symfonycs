<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }


//fonction pour recherche


    public function searchAction(){
   $request = $this->getRequest();
   $data = $request->request->get('search');


   $em = $this->getDoctrine()->getManager();
   $query = $em->createQuery(
    'SELECT p FROM FooTransBundle:Suplier p
    WHERE p.name LIKE :data')
   ->setParameter('data',$data);


$res = $query->getResult();

return $this->render('FooTransBundle:Default:search.html.twig', array(
    'res' => $res));
}
}

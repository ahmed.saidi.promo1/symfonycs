<?php
namespace AppBundle\Controller;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;





use AppBundle\Entity\deplacement;
use AppBundle\Entity\Moyen;
use AppBundle\Entity\Ville;

class depcontroller extends Controller {
    
    /**
     * @Route("/create-deplacement")
     */
    public function createAction(Request $request) {
        
        $deplacement = new deplacement();
        $form = $this->createFormBuilder($deplacement)
            ->add('nom', TextType::class)
            ->add('datedepart', DateType::class, [
            'widget' => 'single_text'
        ])
            ->add('heuredepart', TimeType::class, [
            'widget' => 'single_text'
        ])
            ->add('dateretour', DateType::class, [
            'widget' => 'single_text'
        ])
            ->add('heureretour', TimeType::class, [
            'widget' => 'single_text'
        ])
        ->add('Ville', EntityType::class, [
            'class' => 'AppBundle:Ville',
            'choice_label' => 'nom'
            
        ])
        ->add('Moyen', EntityType::class, [
              'class' => 'AppBundle:Moyen',
              'choice_label' => 'nom'

        ])
            ->add('save', SubmitType::class, [
            'label' => 'Ajouter'
        ])
            ->getForm();
                                
          $form->handleRequest($request);
        if ($form->isSubmitted()) {

            $deplacement = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist($deplacement);
            $em->flush();

            return $this->redirect('/view-deplacement/' . $deplacement->getId());
        }
        return $this->render('deplacement/edit.html.twig', [
            'form' => $form->createView()
        ]);
    }
    
    /**
     * @Route("/view-deplacement/{id}")
     */
    public function viewAction($id) {
        
        $deplacement = $this->getDoctrine()
        ->getRepository(deplacement::class)
        ->find($id);
        
        if (!$deplacement) {
            throw $this->createNotFoundException(
                'Pas de déplacement trouvé avec un id : ' . $id
                );
        }
        
        return $this->render('deplacement/view.html.twig',[
                        'deplacement' => $deplacement
            ]);
        
    }
    /**
     * @Route("/show-deplacements")
     */
    public function showAction() {
        
        $deplacement = $this->getDoctrine()
        ->getRepository(deplacement::class)
            ->findAll();

        return $this->render('deplacement/show.html.twig', array(
            'deplacement' => $deplacement
        ));
    }
    /**
     * @Route("/delete-deplacement/{id}")
     */
    public function deleteAction($id) {
        $em = $this->getDoctrine()->getManager();
        $deplacement = $em->getRepository(deplacement::class)->find($id);

        if (! $deplacement) {
            throw $this->createNotFoundException('Pas de déplacement trouvé avec un id: ' . $id);
        }

        $em->remove($deplacement);
        $em->flush();

        return $this->redirect('/show-deplacements');
        
    }
    /**
     * @Route("/update-deplacement/{id}")
     */
    public function updateAction(Request $request, $id) {
        
        $em = $this->getDoctrine()->getManager();
        $deplacement = $em->getRepository(deplacement::class)->find($id);
        
        if (!$deplacement) {
            throw $this->createNotFoundException(
                'Pas de déplacement trouvé avec un id: ' . $id
                );
        }
        $form = $this->createFormBuilder($deplacement)
            ->add('nom', TextType::class)
            ->add('datedepart', DateType::class, [
            'widget' => 'single_text'
        ])
            ->add('heuredepart', TimeType::class, [
            'widget' => 'single_text'
        ])
            ->add('dateretour', DateType::class, [
            'widget' => 'single_text'
        ])
            ->add('heureretour', TimeType::class, [
            'widget' => 'single_text'
        ])
        ->add('Ville', EntityType::class, [
            'class' => 'AppBundle:Ville',
            'choice_label' => 'nom'
            
        ])
        ->add('Moyen', EntityType::class, [
            'class' => 'AppBundle:Moyen',
            'choice_label' => 'nom'
            
        ])
            ->add('save', SubmitType::class, [
            'label' => 'Editer'
        ])
            ->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted()) {

            $deplacement = $form->getData();
            $em->flush();

            return $this->redirect('/view-deplacement/' . $id);
        }

        return $this->render('deplacement/edit.html.twig', [
            'form' => $form->createView()
        ]);
    }
}

?>